<!doctype html>
<html lang="en-us">

    @include('principal.header')

    <body id='top'>
        @include('principal.parciales.nav')

        @include('principal.parciales.slider')

        <!-- secccion contenido -->

        @yield('contenido')

        <!-- footer-section -->
        @include('principal.footer')

        <!-- Javascript -->
        @include('principal.scripts')

    </body>
</html>