<footer>
<div class="container">

	

	<div class="col-md-3 footer-about">
		<a class="logo" ><img src="{{asset('img/LogoUneg_B.png')}}" height="100px" alt="logo" style="margin-top:8px;"></a>
		
			<ul class="social-icons" style="margin-left:20%" >
					<li><a href="#" class="fa fa-twitter"></a></li>
					<li><a href="#" class="fa fa-facebook"></a></li>
			</ul>
		
	</div>

	<div class="col-md-6 footer-contact-info">
		<h3 class='footer-title'>
			Contactos
			<list class="dropdown">
				<a class='fa fa-plus dropdown-toggle' data-toggle="dropdown" style="margin-left:8px;"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Otros contactos</a></li>
					</ul>
			</list>
		</h3> 
		<p class="website-fax">
			<i class="fa fa-home" style="height:60px;"></i> <span style="width:400px;" class="pull-right">Edificio General de Seguros, Avenida Las Américas Puerto Ordaz, Estado Bolívar - Venezuela.</span> 
		</p>
		<p class="website-number" style="margin-right:80px;">
			<i class="fa fa-phone"></i> +58 (0286) 7137131
		</p>
		<p class="website-email">
			<i class="fa fa-envelope"></i> <a href="#">cace@uneg.edu.ve</a>
		</p>
	</div>
	
	<div class="col-md-3 footer-recent-posts">
		<h3 class='footer-title'>Nuestros Portales</h3>
		<ul>
			<li><a href="#"><i class="fa fa-arrow-circle-right"></i> Pregrado</a></li>
			<li><a href="#"><i class="fa fa-arrow-circle-right"></i> Postgrado</a></li>
			<li><a href="#"><i class="fa fa-arrow-circle-right"></i> Extensión</a></li>
			<li><a href="#"><i class="fa fa-arrow-circle-right"></i> Secretaría</a></li>
		</ul>
	</div>

</div>

</footer>