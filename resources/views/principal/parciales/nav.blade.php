<header>
<div id="top-strip">
	<div class="container">

		<div class="pull-left">
			<list class="dropdown">
				<a class='glyphicon glyphicon-question-sign dropdown-toggle iconos_nav_tam' data-toggle="dropdown"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Preguntas frecuentes</a></li>
					</ul>
			</list>
			<list class="dropdown">
				<a class='glyphicon glyphicon-map-marker dropdown-toggle iconos_nav_tam' data-toggle="dropdown"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Mapa</a></li>
					</ul>
			</list>
			<list class="dropdown">
				<a class='glyphicon glyphicon-envelope dropdown-toggle iconos_nav_tam' data-toggle="dropdown"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Correo Docente/Administrativo</a></li>
						<li><a href="#">Correo Estudiantil</a></li>
					</ul>
			</list>
			<list class="dropdown">
				<a class='glyphicon glyphicon-earphone dropdown-toggle iconos_nav_tam' data-toggle="dropdown"></a>
					<ul class="dropdown-menu">
						<li><a href="#">Contacto</a></li>
						<li><a href="#">Control de Estudios</a></li>
					</ul>
			</list>
			<list class="dropdown">
				<a class='telefono' data-toggle="dropdown" style="margin-left:-7px">+58 (0286) 7137131</a>
			</list>
		</div>

		<div class='pull-right'>
				<div class="input-group col-md-12 imput-lupa">
					<input id="campo_busqueda" type="text" class="search-query form-control tam_bus" placeholder="En Protal UNEG"  />
					<span class="input-group-btn">
						<button class="btn btn-primary tam_bus" type="button">
							<span id="imag_lupa" class="glyphicon glyphicon-search" style="font-size:14px;"></span>
						</button>
					</span>
				</div>
		</div>

	</div>
</div>
</header>
<!-- /Header -->