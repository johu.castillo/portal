<div class="slider-section">
	<div style="background-image: url({{asset('img/d.jpg')}});">	
		<div id="premium-bar">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" style="padding:0px">
							<img class="pequeño" height="60" src="{{asset('img/uneg_logo_blanco.png')}}" alt="logo" >
						</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<!-- Incluye los dropdown's (las listas desplegables) -->
							@include('principal.parciales.dropdown.institucion')
							@include('principal.parciales.dropdown.academia')
							@include('principal.parciales.dropdown.estudiantes')
							@include('principal.parciales.dropdown.sistemaEnLinea')
							@include('principal.parciales.dropdown.informacionUniv')
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container-fluid -->
				</nav>
			</div>
		</div>
	</div>
	<!-- Slider-Section -->
	<div class="main-flexslider">
		<ul class="slides">
			<li class='slides' >
				<img src="img/j.jpg" alt="slide 01">
			</li>
			<li class='slides' >
				<img src="img/d.jpg" alt="slide">
			</li>
			<li class='slides' >
				<img src="img/p.jpg" alt="slide">
			</li>
		</ul>
	</div>
</div>